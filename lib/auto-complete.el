

(defun auto-complete-init ()
  "Initilize auto complete library and process for Habij."
  (interactive)
  (require 'company)
  (global-company-mode)
  (add-to-list 'company-backends '(company-capf company-dabbrev))
  (message "Auto complete module has been loaded"))


(provide 'auto-complete)
