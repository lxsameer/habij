(require 'package)

(defun pkgmgr-package-install (packages)
  "Make sure that the given PACKAGES are installed in the system."
  (mapcar 'package-install packages))

(defun pkgmgr-init ()
  "Initilize the package manager to Habij."
  (setq package-user-dir "~/src/habij/packages")
  (add-to-list 'package-archives
	       '("melpa" . "http://melpa.org/packages/"))
  (package-initialize)
  (unless (file-directory-p package-user-dir)
    (package-refresh-contents)))

(provide 'pkgmgr)
