;; paredit
;; parinfer-mode
;; raindbow-delimiters

(defun setup--elisp ()
  (require 'paredit)
  (require 'parinfer)
  (require 'rainbow-delimiters)
  (paredit-mode t)
  (parinfer-mode t)
  (rainbow-delimiters-mode t))

(defun load-elisp-setup ()
  "Load elisp setup."
  (interactive)
  (add-hook 'emacs-lisp-mode-hook 'setup--elisp)
  (message "Elisp setup has been done!"))

(provide 'elisp)
