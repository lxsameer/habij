
(defun editor-init-ido ()
  (require 'ido)
  (require 'ido-vertical-mode)

  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (ido-mode 1)
  (ido-vertical-mode 1))


(defun global-config ()
  ;; Spaces instead of \t
  (setq-default indent-tabs-mode nil)

  ;; The default indent size
  (setq tab-width 2)

  (add-hook 'before-save-hook 'delete-trailing-whitespace)

  ;; Use Y and N instead of yes and no
  (defalias 'yes-or-no-p 'y-or-n-p))


(provide 'editor)
