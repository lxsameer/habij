# Habij Editor
Super simple editor based on Emacs. I'm creating Habij as an example project for my
[Emacs screencast series](https://www.youtube.com/playlist?list=PLlONLmJCfHTreXptL9k7AMiOTf90t4TRG).

There is a branch for each episode (e.g `ep3`) and the master branch always contains
the changes for the last episode.

## Get Started
In order to use **Habij**, you need `emacs >= 25`. Just use the launcher script (`habij` in
the root) to start the editor.