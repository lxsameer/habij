;;; Habij --- A very simple PoC editor on top of Emacs
;;; Commentary:
;;; Code:
(add-to-list 'load-path "~/src/habij/lib")
(setq debug-on-error t)

(require 'screencast)
(require 'pkgmgr)
(require 'elisp)
(require 'editor)
(require 'auto-complete)

(screencast-init)
;; Package management
(pkgmgr-init)

(pkgmgr-package-install
 '(json-mode
   paredit
   parinfer
   centaur-tabs
   flycheck
   company
   dracula-theme
   rainbow-delimiters
   ido-vertical-mode))

(load-theme 'dracula t)
(load-elisp-setup)

(editor-init-ido)

;; Autocomplete
(auto-complete-init)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-config)

(global-flycheck-mode)
;;; init.el ends here
